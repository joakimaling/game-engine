package test;

import engine.core.Engine;

public class Game {
	public static final String ICON = "/graphics/icon1.jpg";
	public static final String TITLE = "Nyaa Novel";
	public static final String VERSION = "0.1.0";

	public static void main(String[] args) {
		new Engine().start();
	}
}
