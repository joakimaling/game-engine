package engine.game;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

import engine.Engine;
import engine.game.entities.Tile;

public class World {
	private BufferedImage tileSheet;
	private Tile[] tileGrid;
	private int width = 0, height = 0;

	public void fromFile(String file) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(file)))) {
			String currentLine;
			String[] tokens;
			int y = 0;

			if ((currentLine = br.readLine()) != null) {
				tokens = currentLine.trim().split("\\s+", 3);

				width = Integer.parseInt(tokens[0]);
				height = Integer.parseInt(tokens[1]);
				tileSheet = getTileSheet(tokens[2]);

				int tilesPerRow = tileSheet.getWidth() >> Tile.BITS;

				tileGrid = new Tile[width * height];

				while ((currentLine = br.readLine()) != null) {
					tokens = currentLine.trim().split("\\s+");

					for (int x = 0; x < width; x++) {
						int tileX = Integer.parseInt(tokens[x]) % tilesPerRow;
						int tileY = Integer.parseInt(tokens[x]) / tilesPerRow;

						BufferedImage image = tileSheet.getSubimage(tileX << Tile.BITS, tileY << Tile.BITS, Tile.SIZE,
								Tile.SIZE);
						tileGrid[x + y * width] = new Tile(image, (double) (x << Tile.BITS), (double) (y << Tile.BITS));
					}

					y++;
				}
			}
		}
		catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
	}

	private BufferedImage getTileSheet(String sheet) {
		BufferedImage image = null;

		try {
			image = ImageIO.read(getClass().getResourceAsStream(sheet));
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		return image;
	}

	public void draw(Graphics2D g2d, double playerX, double playerY) {
		int offsetMaxX = width << Tile.BITS - Engine.WIDTH;
		int offsetMaxY = height << Tile.BITS - Engine.HEIGHT;

		int cameraX = (int) playerX - Engine.WIDTH >> 1;
		int cameraY = (int) playerY - Engine.HEIGHT >> 1;

		if (cameraX > offsetMaxX) {
			cameraX = offsetMaxX;
		}
		else if (cameraX < 0) {
			cameraX = 0;
		}
		
		if (cameraY > offsetMaxY) {
			cameraY = offsetMaxY;
		}
		else if (cameraY < 0) {
			cameraY = 0;
		}

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				tileGrid[x + y * width].draw(g2d, cameraX, cameraY);
			}
		}
	}
}
