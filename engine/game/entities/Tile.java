package engine.game.entities;

import java.awt.image.BufferedImage;

import engine.game.entities.Entity;

public class Tile extends Entity {
	public static final int SIZE = 32;
	public static final int BITS = 5;

	public Tile(BufferedImage image, double x, double y) {
		super(image, x, y);
	}
}
