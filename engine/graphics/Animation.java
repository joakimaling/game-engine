package engine.graphics;

import java.awt.image.BufferedImage;

public class Animation {
	private BufferedImage[] images;
	private int currentImage = 0;

	private long now = System.nanoTime();
	private long delay;

	public Animation(BufferedImage[] images, long delay) {
		this.images = images;
		this.delay = delay;
	}

	public synchronized void tick() {
		if ((System.nanoTime() - now) / 1000000L > delay) {
			currentImage = ++currentImage % images.length;
			now = System.nanoTime();
		}
	}

	public synchronized BufferedImage getImage() {
		return images[currentImage];
	}
}
