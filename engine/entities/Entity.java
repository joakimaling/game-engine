package engine.entities;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.Rectangle;

public abstract class Entity {
	protected BufferedImage image;
	protected Rectangle collisionSurface;
	protected double x, y;

	public Entity(BufferedImage image, double x, double y, Rectangle collisionSurface) {
		this.image = image;
		this.collisionSurface = collisionSurface;
		this.x = x;
		this.y = y;
	}

	public void draw(Graphics2D g2d, int translateX, int translateY) {
		g2d.drawImage(image, (int) x - translateX, (int) y - translateY, null);
	}

	public boolean isPenetrable() {
		return false;
	}
}
