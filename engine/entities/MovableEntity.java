package engine.entities;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public abstract class MovableEntity extends Entity {
	private static enum Movement { IDLE, NORTH, EAST, SOUTH, WEST };
	protected Movement movement;
	private double velocity;
//		private Animation walkingAnimation;
//		private double walkingSpeed;

	public MovableEntity(BufferedImage image, double x, double y, Rectangle collisionSurface, double velocity) {
		super(image, x, y, collisionSurface);
		this.velocity = velocity;
		movement = Movement.IDLE;
//		walkingAnimation = new Animation(images, 100L);
	}

	public void tick(double delta) {
		int dx = 0, dy = 0;

		switch (movement) {
			case NORTH:
				dy = -1;
				break;
			case EAST:
				dx = -1;
				break;
			case SOUTH:
				dy = 1;
				break;
			case WEST:
				dx = 1;
				break;
			default:
				break;
		}
//		if (directionX != 0 && directionY != 0) {
//			walkingAnimation.tick();
		//	image = walkingAnimation.getImage();
//		}
		x += dx * velocity * delta;
		y += dy * velocity * delta;
	}

	public boolean collidesWith(Entity entity) {
		return collisionSurface.intersects(entity.collisionSurface);
	}
}
