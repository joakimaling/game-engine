package engine.entities;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import engine.entities.Entity;

public class Tile extends Entity {
	public static final int SIZE = 32;

	public Tile(BufferedImage image, double x, double y) {
		super(image, x, y, new Rectangle((int) x, (int) y, SIZE, SIZE));
	}

	// NOTE: have or draw() does it?
	public BufferedImage getImage() {
		return image;
	}
}
