package engine.states;

import java.awt.Graphics2D;

public abstract class State {
	protected StateManager stateManager;

	public State(StateManager stateManager) {
		this.stateManager = stateManager;
	}

	public abstract void draw(Graphics2D g2d);

	public abstract void start();

	public abstract boolean stop();

	public abstract void tick(double delta);

	public abstract void keyPressed(int key);

	public abstract void keyReleased(int key);

	public abstract void mouseButton(int button);

	public abstract void mouseMoved(int x, int y);
}
