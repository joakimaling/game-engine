package engine.states;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import engine.entities.Entity;
import engine.entities.MovableEntity;

public class GameState extends State {
	private ArrayList<Entity> entities = new ArrayList<Entity>();
	// private World world = new World();
	// private Player player;

	// private double tempX = 200, tempY = 200;
	private boolean isPaused = false;

	public GameState(StateManager stateManager) {
		super(stateManager);
	}

	@Override
	public void draw(Graphics2D g2d) {
	}

	@Override
	public void start() {
	}

	@Override
	public boolean stop() {
		return true;
	}

	@Override
	public void tick(double delta) {
//		if (!isPaused) {
//			for (Entity entity : entities) {
//				if (entity instanceof MovableEntity) {
//					((MovableEntity) entity).tick(delta);
//				}
//			}
//		}
	}

	@Override
	public void keyPressed(int key) {
		if (!isPaused) {
			// player.keyPressed(keyCode);
		}

		if (key == KeyEvent.VK_SPACE) {
			isPaused = !isPaused;
		}
	}

	@Override
	public void keyReleased(int key) {
		// player.keyReleased(keyCode);
	}

	@Override
	public void mouseButton(int button) {
	}

	@Override
	public void mouseMoved(int x, int y) {
	}
}
