package engine.states;

import java.awt.Graphics2D;

public class SplashState extends State {

	public SplashState(StateManager stateManager) {
		super(stateManager);
	}

	@Override
	public void draw(Graphics2D g2d) {
	}

	@Override
	public void start() {
	}

	@Override
	public boolean stop() {
		return true;
	}

	@Override
	public void tick(double delta) {
	}

	@Override
	public void keyPressed(int key) {
	}

	@Override
	public void keyReleased(int key) {
	}

	@Override
	public void mouseButton(int button) {
	}

	@Override
	public void mouseMoved(int x, int y) {
	}
}
