package engine.states;

import java.awt.Graphics2D;
import java.util.Stack;

public class StateManager {
	private Stack<State> states = new Stack<State>();

	public StateManager() {
		push(new SplashState(this));
	}

	/**
	 * Renders the game state.
	 *
	 * @param g2d
	 */
	public void draw(Graphics2D g2d) {
		states.peek().draw(g2d);
	}

	/**
	 * Pushes a game state onto the stack and activates it.
	 *
	 * @param state
	 */
	public void push(State state) {
		states.push(state);
	}

	/**
	 * Pops a game state off the stack, activating the previous game state.
	 *
	 * @return
	 */
	public State pop() {
		return states.pop();
	}

	/**
	 * Runs the initialisation method of the state.
	 */
	public void start() {
		states.peek().start();
	}

	/**
	 * Runs the finalisation method of the state.
	 */
	public boolean stop() {
		return states.peek().stop();
	}

	/**
	 * Updates the game state.
	 *
	 * @param delta
	 */
	public void tick(double delta) {
		states.peek().tick(delta);
	}

	public void keyPressed(int key) {
		// states.peek().keyPressed(key);
	}

	public void keyReleased(int key) {
		// states.peek().keyReleased(key);
	}

	public void mouseButton(int button) {
		// states.peek().mouseButton(button);
	}

	public void mouseMoved(int x, int y) {
		// states.peek().mouseMoved(x, y);
	}
}
