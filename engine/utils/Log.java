package engine.utils;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
	private static void base(PrintStream stream, String type, int colour, String message) {
		stream.printf(
			"\u001B[%dm[%s] %s: %s\u001B[0m\n",
			colour,
			new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()),
			type,
			message
		);
	}

	public static void error(final String message) {
		base(System.err, "ERROR", 31, message);
	}

	public static void info(final String message) {
		base(System.out, "INFO", 0, message);
	}

	public static void warning(final String message) {
		base(System.out, "WARNING", 33, message);
	}
}
