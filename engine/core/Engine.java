package engine.core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import engine.states.StateManager;
import engine.utils.Log;

public class Engine extends JPanel implements Runnable {
	public static final String TITLE = "Maelstrom";
	public static final String VERSION = "0.0.1";
	public static final String ICON = "/graphics/icon1.jpg";
	public static final int WIDTH = 480;
	public static final int HEIGHT = WIDTH / 12 * 9;
	public static final double SCALE = 2D;

	// Engine specific
	private static final long serialVersionUID = -7036277749452318403L;
	private volatile boolean isRunning = false;
	private JFrame frame = new JFrame(TITLE);
	private Thread thread;

	// Graphics specific
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private Graphics2D g2d = (Graphics2D) image.getGraphics();

	// Game specific
	private StateManager stateManager = new StateManager();

	public Engine() {
		Log.info("Initialises the engine");
		g2d.setBackground(Color.BLACK);

		// Add the listeners
		addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {}

			@Override
			public void mouseMoved(MouseEvent e) { // TODO: replace with current scale
				stateManager.mouseMoved((int) (e.getX() / SCALE), (int) (e.getY() / SCALE));
			}
		});
		addMouseListener(new MouseListener() {
			@Override
		    public void mouseClicked(MouseEvent e) {}

			@Override
		    public void mouseEntered(MouseEvent e) {}

			@Override
		    public void mouseExited(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {
				stateManager.mouseButton(e.getButton());
		    }

			@Override
		    public void mouseReleased(MouseEvent e) {
				stateManager.mouseButton(0);
		    }
		});
		addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				stateManager.keyPressed(e.getKeyCode());
			}

			@Override
			public void keyReleased(KeyEvent e) {
				stateManager.keyReleased(e.getKeyCode());
			}

			@Override
			public void keyTyped(KeyEvent e) {}
		});

		// Setup the inner "canvas"
		setPreferredSize(new Dimension((int) (WIDTH * SCALE), (int) (HEIGHT * SCALE)));
		setDoubleBuffered(true);
		setFocusable(true);
		requestFocus();

		// Setup the outer frame
		frame.setContentPane(this);
		frame.pack();
		frame.setIconImage(new ImageIcon(getClass().getResource(ICON)).getImage());
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);

		// If the user clicked the close button the state manager's stop method will be executed in
		// order for the user to save their data.
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (stateManager.stop()) {
					stop();
					System.exit(0);
				}
			}
		});
	}

	/**
	 * Returns the name and version of the engine.
	 *
	 * @return String
	 */
	public String getVersion() {
		return TITLE + " v" + VERSION;
	}

	/**
	 * Returns the state of whether the engine is maximised or not.
	 *
	 * @return boolean
	 */
	public boolean isFullscreen() {
		return frame.getExtendedState() == JFrame.MAXIMIZED_BOTH;
	}

	/**
	 * Returns the running state of the engine.
	 *
	 * @return boolean
	 */
	public synchronized boolean isRunning() {
		return isRunning;
	}

	/**
	 * Toggles the engine between fullscreen and windowed mode by scaling it up to fit the current
	 * screen size.
	 *
	 * @param boolean state
	 */
	public void setFullscreen(boolean state) {
		double scale = state ? Toolkit.getDefaultToolkit().getScreenSize().width / WIDTH : SCALE;

		setPreferredSize(new Dimension((int) (WIDTH * scale), (int) (HEIGHT * scale)));
		frame.setExtendedState(state ? JFrame.MAXIMIZED_BOTH : JFrame.NORMAL);
		frame.setUndecorated(state);
	}

	/**
	 * Starts the engine by creating a thread. Since the engine implements Runnable execution will
	 * continue in the run method.
	 */
	public synchronized void start() {
		if (!isRunning()) {
			Log.info("Starting the engine");
			isRunning = true;
			thread = new Thread(this, getVersion());
			thread.setPriority(Thread.MAX_PRIORITY);
			thread.start();
		}
	}

	/**
	 * Stops the engine by exiting the run method and joining the thread.
	 */
	public synchronized void stop() {
		if (isRunning()) {
			try {
				Log.info("Stopping the engine");
				isRunning = false;
				thread.join();
			}
			catch (InterruptedException e) {
				Log.error(e.toString());
			}
		}
	}

	/**
	 * Calls the state manager to do its calculations of entity positions, collisions, score and
	 * other things. Passing the delta value to adjust the calculations to the speed of the CPU.
	 *
	 * @param double
	 */
	private void tick(double delta) {
		stateManager.tick(delta);
	}

	/**
	 * Renders the image fetched from the state manager, onto the engine, canvas and clears it for a
	 * new image to be drawn.
	 */
	private void draw() {
		stateManager.draw(g2d);

		Graphics g = (Graphics) getGraphics();
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		g.dispose();

		g2d.clearRect(0, 0, getWidth(), getHeight());
	}

	@Override
	public void run() {
		long now, sleepTime, lastLoopTime = System.nanoTime();
		final long OPTIMAL_TIME = 1000000000L / 60L;

		while (isRunning()) {
			now = System.nanoTime();

			lastLoopTime = now;

			tick((now - lastLoopTime) / (double) OPTIMAL_TIME);

			draw();

			if ((sleepTime = (lastLoopTime - System.nanoTime() + OPTIMAL_TIME) / 1000000L) > 0) {
				try {
					Thread.sleep(sleepTime);
				}
				catch (InterruptedException e) {
					Log.error(e.toString());
				}
			}
		}
	}

	public static void main(String[] args) {
		new Engine().start();
	}
}
